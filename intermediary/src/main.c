/**
 * @file main.c
 * @author LCY
 * @date 2020.03.09
 * @brief 主程式執行區段。
 *
 * *Notes:
 *  Fcpu = 16Mhz ---> 1clock = 62.5ns
 *  Reset所需時長: 400ns --> 約7個clock
 */

#include <avr/io.h>
#include <stdio.h>

#include "Descriptors.h"
#include "DualVirtualSerial.h"
#include "comu.h"
#include "interrupt.h"
#include "iostate.h"
#include "packet_handler.h"

void comu_step(void);
void ctl_step(void);

static uint8_t state = 99;
static uint8_t soft_switch = SOFT_OFF;
static uint8_t Buffer[300];
static PacContainer_t Pc = {.data = Buffer};
static PacDecoder_t Pd = {.pac_p = &Pc};

ISR(USART1_RX_vect) {
    // FIXME: COMPORT 斷線後仍然不斷觸發
    // From issue #5
    volatile uint8_t data = UDR1;
    if (CDC_comu.State.ControlLineStates.HostToDevice == 3) {
        CDC_Device_SendByte(&CDC_comu, data);
        rx_led_on();
    }
}

ISR(USART1_TX_vect) {
    tx_led_on();
}

ISR(INT0_vect) {
    if(soft_switch == SOFT_OFF) {
        state = p_r_detect();
        p_r_update(state);

        if (state == PR_MODE_PROG) {
            state = m_s_detect();
            m_s_update(state);
        }
    }
}

ISR(INT1_vect) {
    uint8_t state;

    state = m_s_detect();
    m_s_update(state);
}

ISR(TIMER1_COMPA_vect) {
    tx_led_off();
    rx_led_off();
}

void setup_hardware(void) {
    /* Disable watchdog if enabled by bootloader/fuses */
    MCUSR &= ~(1 << WDRF);
    wdt_disable();

    /* Disable clock division */
    clock_prescale_set(clock_div_1);

    ex_init();
    tim_init();
    gpio_init();
    USB_Init();
}

int main(void) {
    setup_hardware();
    //initially set PD2 PD3 Hi-Z to prevent UART comptition
    //if USB plugs in, the "EVENT_USB_Device_Connect" will trigger uart initial
    DDRD &= ~(3 << 2);
    PORTD |= (3 << 2);
    MCUCR |= (1 << 4);
    sei();

    for (;;) {
        comu_step();
        ctl_step();
    }
}

void comu_step(void) {
    int16_t ReceivedByte;

    ReceivedByte = CDC_Device_ReceiveByte(&CDC_comu);
    if (!(ReceivedByte < 0)) {
        uart_put((uint8_t)ReceivedByte);
    }

    CDC_Device_USBTask(&CDC_comu);
    USB_USBTask();
}

void ctl_step(void) {
    uint8_t cmd;
    int16_t ReceivedByte;

    ReceivedByte = CDC_Device_ReceiveByte(&CDC_ctl);
    if (!(ReceivedByte < 0)) {
        packet_decoder_step(&Pd, (uint8_t)ReceivedByte);
    }

    if (packet_decoder_is_done(&Pd)) {
        soft_switch = SOFT_ON;
        cmd = p1_cmd_decode(&Pc);
        if (cmd == P1_CMD_RESET) {
            reset_u1();
            CDC_Device_SendString(&CDC_ctl, "Reset target.\n\r");
        }
        if (cmd == P1_CMD_TO_PROG) {
            to_prog_u1();
            reset_u1();
            CDC_Device_SendString(&CDC_ctl, "Switch to Program mode + Reset target. \n\r");
        }
        if (cmd == P1_CMD_TO_RUN) {
            to_run_u1();
            CDC_Device_SendString(&CDC_ctl, "Switch to Run mode.\n\r");
        }
        if (cmd == P1_CMD_TO_RUN_RESET) {
            to_run_u1();
            reset_u1();
            CDC_Device_SendString(&CDC_ctl,"Switch to Run mode + Reset target. \n\r");
        }
        packet_decoder_unlock(&Pd);
        soft_switch = SOFT_OFF;
    }

    CDC_Device_USBTask(&CDC_ctl);
    USB_USBTask();
}
