/**
 * @file iostate.h
 * @author LCY
 * @author LiYu87@mvmc-lab
 * @date 2020.03.09
 * @brief atmega16U4相關設定以及狀態偵測定義。
 * 
 * Note:
 *  旋鈕:                                   Slave       Master
 *      A0                  PF4               o           x
 *      A1                  PF5               o           x
 *      A2                  PF6               o           x
 *      A3                  PF7               o           x
 *
 *  指撥開關:
 *      M/S                 PF1               o           o
 *
 *  M/S控制腳               PD6               o           o
 *
 *  U4_CS_SS_TO_128         PD5               o          x
 *  CS/SS_BUS               PD1               o          o
 *
 *
 *  ASA_ID:
 *      Addr0               PB4               o          o
 *      Addr1               PB5               o          o
 *      Addr2               PB6               o          o
 *
 * Note:
 *  Pro/Run                 PF0
 *  Pro/Run_To_128          PD7 / PG1
 */

#ifndef IOSTATE_H
#define IOSTATE_H

#include <inttypes.h>

#define MS_MODE_MASTER 0
#define MS_MODE_SLAVE  1

#define PR_MODE_RUN    0
#define PR_MODE_PROG   1

/**
 * @brief 對 GPIO 做初始化
 * 
 * 初始化的PORT有 PORTF, PORTD, PORTC, PORB
 */
void gpio_init(void);

/**
 * @brief 取得ASABUS上當前的ASAID。
 * 
 * @return uint8_t ASAID(0~7)
 */
uint8_t asa_id_bus(void);

/**
 * @brief 取得ASAID旋鈕上的ASAID。
 * 
 * @return uint8_t ASAID(0~9)
 */
uint8_t asa_id_sw(void);

/**
 * @brief 比較ASABUS與ASAID旋鈕的ID是否吻合。
 * 
 * @return uint8_t 
 *    - 1 : 吻合。
 *    - 0 : 不吻合。
 */
uint8_t is_asa_id_matched(void);

/**
 * @brief 偵測硬體狀態，判斷當前為 Master 或 Slave 模式。
 *
 * @return uint8_t
 *    - MS_MODE_MASTER(0) : Master 模式。
 *    - MS_MODE_SLAVE (1) :  Slave 模式。
 */
uint8_t m_s_detect(void);

/**
 * @brief 更改硬體設置為 Master 或 Slave 模式。
 * 
 * @param mode Master/Slave 模式選擇。
 *    - MS_MODE_MASTER(0) : Master 模式。
 *    - MS_MODE_SLAVE (1) :  Slave 模式。
 */
void m_s_update(uint8_t mode);

/**
 * @brief 偵測硬體狀態，判斷當前為 Prog 或 Run 模式。
 *
 * @return uint8_t
 *    - PR_MODE_RUN  (0) :  Run 模式。
 *    - PR_MODE_PROG (1) : Prog 模式。
 */
uint8_t p_r_detect(void);

/**
 * @brief 更改硬體設置為 Prog 或 Run 模式。
 * 
 * @param mode Prog/Run 模式選擇。
 *    - PR_MODE_RUN  (0) :  Run 模式。
 *    - PR_MODE_PROG (1) : Prog 模式。
 */
void p_r_update(uint8_t mode);

/**
 * @brief 致能TX閃爍提示燈。
 */
void tx_led_on(void);

/**
 * @brief 致能RX閃爍提示燈。
 */
void rx_led_on(void);

/**
 * @brief 禁能RX閃爍提示燈。
 */
void rx_led_off(void);

/**
 * @brief 禁能TX閃爍提示燈。
 */
void tx_led_off(void);

/**
 * @brief 發送一個重置訊號給U1。
 */
void reset_u1(void);

/**
 * @brief 將 Prog/Run 電位拉至 Prog 模式 (high)
 * 
 */
void to_prog_u1(void);

/**
 * @brief 將 Prog/Run 電位拉至 Run 模式 (low)
 * 
 */
void to_run_u1(void);

#endif  /* IOSTATE_H */