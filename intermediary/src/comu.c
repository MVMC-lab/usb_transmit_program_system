/**
 * @file comu.c
 * @author LCY
 * @date 2020.03.09
 * @brief 通訊相關函式實作。
 */

#include "comu.h"

/* 通訊相關參數設定 */
#define BAUDRATE 38400UL
#define UBRR_VAL (F_CPU / 16 / BAUDRATE - 1)

void uart_init(void) {
    UBRR1H = (uint8_t)(UBRR_VAL >> 8);
    UBRR1L = (uint8_t)UBRR_VAL;
    UCSR1B = (1 << RXEN1) | (1 << TXEN1);
    UCSR1C = (3 << UCSZ10);
}

void uart_deinit(void) {
    UBRR1 = 0;
    UCSR1B = 0;
    UCSR1C = 0;
}

void uart_put(uint8_t c) {
    while (!(UCSR1A & (1 << UDRE1)))
        ;
    UDR1 = c;
}