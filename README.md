# USB_Transmit_Program_System

The following is the library and license used in this project:
- [LUFA](https://github.com/abcminiuser/lufa) - LUFA.license

## COMPORT 說明

M128插上後會顯示兩個COMPORT，如果有安裝裝置描述驅動可以在裝置管理員看到:

![](https://i.imgur.com/bkKhYUF.png)

* 溝通通道: 為傳輸使用的COMPORT，M128與PC之間的資料都經由此COMPORT傳輸
* 命令通道: 下命令專用的COMPORT，在這邊可以用命令的方式reset以及更改program/run模式

**如果沒有安裝的話就不會有中文名字，請嘗試看看確認哪個是溝通，哪個是命令**

## 命令使用說明

1. 連接命令通道
2. 輸入指令:
    * 可使用指令:
        * `!reset`  : 重置
        * `!run`    : 切換為執行模式(需要再執行一次`!reset`重置)
        * `!prog`   : 切換為燒錄模式

## 使用範例

* `COM6`: 命令
* `COM7`: 溝通

COM6 顯示結果:

![](https://i.imgur.com/lhq9nCc.png) 

COM7 顯示結果:

![](https://i.imgur.com/58T5rjm.png)
